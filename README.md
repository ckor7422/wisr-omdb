# omdb

A Flutter app that searches for Movies, Series or both, while including persistent favourites functionality.

## Getting Started

The app has currently only been run on an iOS simulator using an iPhone X and a physical Samsung galaxy s10e.

From the root directory of the repo run 

"flutter pub get"

then

"flutter run"

## Requirements

The app should contain the minimum of two screens, a Search Page and a Detail Page:

1) Search Page should allow users to filter the search by Movies, Series or both (tabs, segmented control, etc, you pick)

2) As the Search Page shows its results upon typing the name of the movie/series, a favorite button should appear for each item cell result, allowing users to see what they had favourited before as well as allowing them to favorite/unfavorite the items when tapping the button

3) Upon tapping on the item cell result, users should be taken to the Detail Page where they should see the item's post, title, year, description(bt fetching the item's info by id) and the favourite button4) Favourited items should be persisted between sessions


## Bonus points for:

- Tests!
- Pagination
- Using RxDart
- Favourite listing
- Detailing through comments how you'd handle deeplinking/routing
- Detailing through comments how you'd handle authentication for a potential favourites API

## Files

bloc : contains all the blocs used in the app
- apiBloc.dart : holds all api functions
- appBloc.dart : the base bloc where all other blocs are initialised
- bloc.dart : abstract class just to ensure all blocs have a dispose overide for streams
- blocProvider.dart : class used to search the apps context tree for the appBloc and provide it to pages.
- favouriteBloc.dart : bloc used to track objects favourited in the app.
- searchBloc.dart : bloc used to search, share to pages and hold results returned from the api
- movieListModal.dart : holds function used to combine the favourites and search search results streams
- navigationBloc.dart : bloc that tracks the base layer of navigation in the app

common : contains files with any objects that are reused in various parts of the app
- sidebar.dart : holds the apps drawer widget

dataTypes : contains any classes for data types
- environment.dart : object for holding app environment variables such as the base of the api endpoint
- objectDetails.dart : object for holds the details of an omdb object displayed in the focused result details page
- omdbObject.dart : object for holding the omdb details of individual movies/series returned from the search by title call.

developerTools : contains any files with functions that are used for developement but are not a part of the final app
- consoleDebugging.dart : holds a function for console print statements with stacktrace tracking

pages : contains the apps screens 
- searchMovies.dart/searchSeries.dart : the page that displays search results
    * these are essentially the same file seperated in order to allow an example of base page navigation
- navigation.dart : paged that provides a switch to allow basepage navigation based on the navigation bloc
- focusResultsDetails.dart : the page used to display the details of an individual omdb object when fetched by id from the omdb api

## Notes:

For reference about 8-9 Hours spent on the app 

- RxDart was used but the function combineLatest2 can probably be replaced with one better suited for the use case it requires both 
combined streams to send a new event before triggering the stream this was worked around by adding uneeded streams when thre was no 
update to the data.

- Deeplinking would likely be handled in its own bloc with a listener placed inside the appBloc. The DeepLinkBloc would be initialised and
be setup to catch data pushed into the app relevant to a deep link after which it would push a stream indicating where in the app the link 
is tryin to push the user to. On catching this the appBloc would make a call to the navigation bloc which would need an additional function
that changes the apps set page to the location indicated.

- Authentication for a potential favourites API would include the following.
* a login bloc :
    This would hold login/out functions as well as functions for persisting the login session details
* a login datatype 
    This would hold the details returned from a login api call such as username, userId, tokken, tokkenExpirationDate
* a get favourites api call in the apiBloc
    This would make a get call with including the users userId and tokken stored in the login bloc an retrieve a list of
    favourited omdb objects from the server.
* the favouritesBloc would need to be updated 
    As it currently stores the list of favourites as a list of strings in shared preferences this would be better stored in sqlite
    as a user may eventually stockpile hundreds of data objects. This could further scale if episode favouriting is added.
