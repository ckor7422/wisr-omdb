import 'package:flutter/material.dart';
import 'package:omdb/bloc/appBloc.dart';
import 'package:omdb/bloc/blocProvider.dart';
import 'package:omdb/bloc/navigationBloc.dart';
import 'package:omdb/bloc/searchBloc.dart';

Widget sideBar(BuildContext context) {
  AppBloc appBloc = BlocProvider.of<AppBloc>(context);
  NavBloc navBloc = appBloc.nav;
  SearchBloc searchBloc = appBloc.searchBloc;

  return Drawer(
    // Add a ListView to the drawer. This ensures the user can scroll
    // through the options in the drawer if there isn't enough vertical
    // space to fit everything.
    child: ListView(
      // Important: Remove any padding from the ListView.
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: Text('User Name Placeholder'),
          decoration: BoxDecoration(
            color: Colors.blueGrey,
          ),
        ),
        ListTile(
          title: Text('Movies'),
          onTap: () {
            searchBloc.clearSearchResults();
            navBloc.setPage = Pages.SearchMovies;
          },
        ),
        ListTile(
          title: Text('Series'),
          onTap: () {
            searchBloc.clearSearchResults();
            navBloc.setPage = Pages.SearchSeries;
          },
        ),
      ],
    ),
  );
}
