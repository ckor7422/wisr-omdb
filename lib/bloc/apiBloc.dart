import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:flutter/services.dart';
import 'package:omdb/dataTypes/environment.dart';
import 'package:yaml/yaml.dart';

import 'appBloc.dart';
import 'bloc.dart';

class APIBloc implements Bloc {
  final AppBloc _appBloc;

  // The api bloc must be passed the app environment as it contains the base api url
  final Environment environment;

  APIBloc(this._appBloc, this.environment);

  Future<String> pulledVersion = rootBundle.loadString("pubspec.yaml");

  // Idealy you would I would not want an api key available in the code
  // and would prefer it is loaded into the app.
  String apiKey = "&apikey=938dee11";

  /// /// /// /// /// /// /// ///
  /// Helper Functions        ///
  /// /// /// /// /// /// /// ///
  Future<String> getAppVersion() async {
    var yaml = loadYaml(await pulledVersion);
    print(yaml["version"]);

    return yaml["version"];
  }

  /// /// /// /// /// /// /// ///
  /// USER API CALLS          ///
  /// /// /// /// /// /// /// ///

  /// Function to search the OMBD open api by title
  /// Returns a Json object containing multiple key values and
  /// an array of smaller json objects referencing movie details.
  Future<http.Response> searchByTitleApi(String title, String type) async {
    http.Response response;
    try {
      if (type.isNotEmpty) {
        response = await http.get(environment.baseApiURL + "?s=" + title + "&type=" + type + apiKey, headers: {});
      } else {
        response = await http.get(environment.baseApiURL + "?s=" + title + apiKey, headers: {});
      }

      checkServerResponse(response);
      return response;
    } catch (e) {
      print(e);
      return e;
    }
  }

  Future<http.Response> searchByIMDBIDApi(String imdbId) async {
    http.Response response;
    try {
      response = await http.get(environment.baseApiURL + "?i=" + imdbId + apiKey, headers: {});

      checkServerResponse(response);
      return response;
    } catch (e) {
      print(e);
      return e;
    }
  }

// function to handle errors detecting/throwing pass jsons through it return responses.
  void checkError(http.Response response) {
    Map<String, dynamic> jsonResponse = jsonDecode(response.body);
    if (jsonResponse['error'] != null) {
      throw new Exception(jsonResponse['error']);
    }
  }

  /// function to check if the server returned an OK response
  bool checkServerResponse(http.Response response) {
    if (response.statusCode == 200) {
      print(response.statusCode.toString() + " Valid Server Response");
    } else if (response.statusCode == 300) {
      print(response.statusCode.toString() + " Server Redirection");
    } else if (response.statusCode == 400) {
      print(response.statusCode.toString() + " Client Errors - App Error");
    } else if (response.statusCode == 500) {
      print(response.statusCode.toString() + " Server Error");
    } else {
      print(response.statusCode.toString() + " Unexpected Error");
      throw Exception('Failed to get valid response from server');
    }
    return true;
  }

  final _controller = StreamController<bool>.broadcast();

  Stream<bool> get appContextStream => _controller.stream;

  @override
  void dispose() {
    _controller.close();
  }
}
