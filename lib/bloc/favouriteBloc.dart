import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

import 'appBloc.dart';
import 'bloc.dart';

/// This needs to hold a list of favourited movies/series
/// They should be tracked by imdbID

/// This bloc should collect search responses and provide a list of
/// movie objects that can be displayed in a list on the search page
class FavouriteBloc implements Bloc {
  // Data that the UserStatusBloc needs to store.
  final AppBloc _appBloc;
  FavouriteBloc(this._appBloc);

  List<String> _favourites = [];

  get getFavourites => _favourites;

  void pushStream() {
    _statusController.sink.add(_favourites);
  }

  void addFavourite(String imdbId) {
    _favourites.add(imdbId);
    persistFavourites();
    _statusController.sink.add(_favourites);
  }

  void removeFavourite(String imdbId) {
    _favourites.removeWhere((element) => element == imdbId);
    _statusController.sink.add(_favourites);
  }

  // creates or overides existing selected favourites
  void persistFavourites() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setStringList("omdb@favourites", _favourites);
  }

  void loadPersistedFavourites() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.containsKey("omdb@favourites")) {
      _favourites = pref.getStringList("omdb@favourites");
    }
  }

  bool isFavourited(String imdbId) {
    if (_favourites.contains(imdbId)) {
      return true;
    } else {
      return false;
    }
  }

  final _statusController = StreamController<List<String>>.broadcast();

  Stream<List<String>> get statusStream => _statusController.stream;

  @override
  void dispose() {
    _statusController.close();
  }
}
