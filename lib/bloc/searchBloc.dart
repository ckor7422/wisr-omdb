/// This bloc should collect search responses and provide a list of
/// movie objects that can be displayed in a list on the search page

import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:omdb/dataTypes/objectDetails.dart';
import 'package:omdb/dataTypes/omdbObject.dart';
import 'package:omdb/developerTools/consoleDebugging.dart';
import 'package:rxdart/rxdart.dart';

import 'appBloc.dart';
import 'bloc.dart';
import 'package:http/http.dart' as http;

class SearchBloc implements Bloc {
  // Data that the UserStatusBloc needs to store.
  final AppBloc _appBloc;
  SearchBloc(this._appBloc);

  List<OMDBObject> _searchResults;

  List<OMDBObject> get getSearchResults => _searchResults;

  OMDBObject _selectedResult;

  ObjectDetails _selectedResultDetails;

  get getSelectedResultDetails => _selectedResultDetails;

  set setSearchResults(List<OMDBObject> sites) {
    _searchResults = sites;
    _statusController.sink.add(_searchResults);
  }

  void clearSearchResults() {
    _searchResults = [];
    _statusController.sink.add(_searchResults);
  }

  void pushStream() {
    _statusController.sink.add(_searchResults);
  }

  set setSelectedResult(OMDBObject selectedResult) {
    _selectedResult = selectedResult;
  }

  void populateSearchResults(String title, String type) async {
    List<OMDBObject> noResult = [];
    noResult.add(OMDBObject(title: "No Result"));
    http.Response response = await _appBloc.apiBloc.searchByTitleApi(title, type);
    if (json.decode(response.body)["Search"] == null) {
      consoleDebugging("Search Bloc ", StackTrace.current, "No matches to search");
      _searchResults = noResult;
      _statusController.sink.add(_searchResults);
    }
    try {
      // looking for the error cause a type difference exception
      // catch this and then proceed as if the data is correct
      final parsed = json.decode(response.body)["Search"].cast<Map<String, dynamic>>();
      final data = parsed.map<OMDBObject>((json) => OMDBObject.fromJson(json)).toList();
      final results = <OMDBObject>[];
      for (var i = 0; i < data.length; i++) {
        results.add(data[i]);
      }
      _searchResults = results;
      _statusController.sink.add(_searchResults);
    } catch (e) {
      consoleDebugging("Search Bloc", StackTrace.current, "Error populating search results", e.toString());
    }
  }

  Future<bool> getSpecificObjectDetails() async {
    http.Response response = await _appBloc.apiBloc.searchByIMDBIDApi(_selectedResult.imdbID);
    try {
      _selectedResultDetails = ObjectDetails.fromJson(jsonDecode(response.body));
      return true;
    } catch (e) {
      consoleDebugging("Search Bloc", StackTrace.current, "Error populating specific result details");
      return false;
    }
  }

  final _statusController = StreamController<List<OMDBObject>>.broadcast();

  Stream<List<OMDBObject>> get statusStream => _statusController.stream;

  @override
  void dispose() {
    _statusController.close();
  }
}
