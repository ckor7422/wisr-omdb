import 'package:omdb/bloc/favouriteBloc.dart';
import 'package:omdb/bloc/movieListModal.dart';
import 'package:omdb/bloc/searchBloc.dart';
import 'package:omdb/dataTypes/environment.dart';
import 'package:omdb/developerTools/consoleDebugging.dart';

import 'apiBloc.dart';
import 'bloc.dart';
import 'navigationBloc.dart';

/// An extra bloc which is used to orchestrate communication between blocs.
///
/// All Blocs are instantiated in the AppBloc and should only be instatiated once
/// then shared accross the application using the bloc_provider.
///
/// In order to access the blocs use the code :
///   AppBloc appBloc = BlocProvider.of<AppBloc>(context);
///
/// after which individual blocs can be accessed using :
///   appBloc.<blocName>
///
/// Create a local instance of the bloc to make code cleaner:
///    <BlocName> <blocName> = appBloc.<blocName>
///
/// Examples found in any of the _page.dart files in the pages folder.
///

class AppBloc implements Bloc {
  // Create blocs used in the app
  NavBloc nav;
  APIBloc apiBloc;
  Environment appEnvironment;
  SearchBloc searchBloc;
  FavouriteBloc favouriteBloc;
  MoviesListViewModel moviesListViewModel;

  AppBloc(Environment environment) {
    // Initialise blocs used in the app
    appEnvironment = environment;
    nav = NavBloc();
    apiBloc = APIBloc(this, environment);
    searchBloc = SearchBloc(this);
    favouriteBloc = FavouriteBloc(this);
    favouriteBloc.loadPersistedFavourites();
    moviesListViewModel = MoviesListViewModel(this);

    // Start the app on the the page set bellow
    nav.setPage = Pages.SearchMovies;

    // Navigation
    nav.pageStream.listen((data) {
      consoleDebugging("NavStream", StackTrace.current, "Page data received");
    }, onDone: () {
      consoleDebugging("NavStream", StackTrace.current, "Page data pushed to main_bloc");
    }, onError: (error) {
      consoleDebugging("NavStream", StackTrace.current, "Error moving data between blocs.");
    });

    // Favourites
    favouriteBloc.statusStream.listen((data) {
      consoleDebugging("FavouriteStream", StackTrace.current, "newStream");
    }, onDone: () {}, onError: (error) {});

    // Searchs
    // Pushing a favourites stream was needed because the combineLatest2 function
    // requires both streams to push before it triggers. This is not optimal and a
    // better replacement need to be found.
    searchBloc.statusStream.listen((data) {
      consoleDebugging("SearchStream", StackTrace.current, "newStream");
      favouriteBloc.pushStream();
    }, onDone: () {}, onError: (error) {});
  }

  @override
  void dispose() {
    nav.dispose();
    apiBloc.dispose();
    searchBloc.dispose();
    favouriteBloc.dispose();
    moviesListViewModel.dispose();
    apiBloc.dispose();
  }
}
