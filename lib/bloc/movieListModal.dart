import 'dart:async';

import 'package:omdb/bloc/appBloc.dart';
import 'package:omdb/bloc/bloc.dart';
import 'package:omdb/dataTypes/omdbObject.dart';
import 'package:rxdart/rxdart.dart';

class FavouritedMovies {
  OMDBObject movie;
  bool favourited;
  FavouritedMovies({this.movie, this.favourited});
}

// TODO: This bloc needs to be reviewed if it is needed.
class MoviesListViewModel implements Bloc {
  final AppBloc appBloc;

  MoviesListViewModel(this.appBloc);

  /// returns the entire movies list with user-favourite information
  Stream<List<FavouritedMovies>> moviesUserFavouritesStream() {
    return Rx.combineLatest2(appBloc.searchBloc.statusStream, appBloc.favouriteBloc.statusStream,
        (List<OMDBObject> movies, List<String> userFavourites) {
      List<FavouritedMovies> combinationList = [];
      for (OMDBObject movie in movies) {
        if (userFavourites.contains(movie.imdbID)) {
          combinationList.add(FavouritedMovies(movie: movie, favourited: true));
        } else {
          combinationList.add(FavouritedMovies(movie: movie, favourited: false));
        }
      }
      if (combinationList.isNotEmpty) {
        return combinationList;
      }
    });
  }

  // disposen uneeded as no stream stored here.
  @override
  void dispose() {}
}
