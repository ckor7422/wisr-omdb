// All BLoC classes will conform to this interface,
// this forces them to add a dispose method.

abstract class Bloc {
  void dispose();
}
