// A bloc used to manage navigation between pages

// This bloc has as context stream that can be used on widgets that
// need to change dyncamically based on the app context.

import 'dart:async';

import 'package:flutter/material.dart';

import 'bloc.dart';

enum Pages {
  Login,
  Home,
  SearchMovies,
  SearchSeries,
  Profile,
  Invites,
  Initialising,
  Onboarding,
  SignUp,
}

class NavBloc implements Bloc {
  // Data that the navBloc needs to store.

  // The navigation bloc should hold the current context of the app at all times
  // this will allow the correct pushes and pops between pages to occur.
  BuildContext _appContext;
  Pages _page;

  BuildContext get getAppContext => _appContext;
  Pages get getPage => _page;

  set setContext(BuildContext context) {
    _appContext = context;
    _appContextController.sink.add(context);
  }

  set setPage(Pages page) {
    _page = page;
    _pageController.sink.add(page);
  }

  final _appContextController = StreamController<BuildContext>.broadcast();
  final _pageController = StreamController<Pages>.broadcast();

  Stream<BuildContext> get appContextStream => _appContextController.stream;
  Stream<Pages> get pageStream => _pageController.stream;

  @override
  void dispose() {
    _appContextController.close();
    _pageController.close();
  }
}
