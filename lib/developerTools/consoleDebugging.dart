import 'package:flutter/widgets.dart';

/// Function used to debug to the console with identifier location details and
/// message
void consoleDebugging(String uniqueIdentifier, StackTrace trace, String message, [String nextLine = ""]) {
  String locationAndLine = trace.toString().split("\n")[0];
  String temp = locationAndLine.split("(")[1];
  locationAndLine = temp.replaceRange(temp.length - 1, temp.length, " :: ");

  debugPrint(uniqueIdentifier + ":: " + locationAndLine + "" + message + ": ");

  if (nextLine != "") {
    debugPrint(uniqueIdentifier + ": " + nextLine);
  }
}
