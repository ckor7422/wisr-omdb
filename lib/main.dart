import 'package:flutter/material.dart';
import 'package:omdb/pages/navigation.dart';

import 'bloc/appBloc.dart';
import 'bloc/blocProvider.dart';
import 'dataTypes/environment.dart';

void main() {
  runApp(OMDBApp());
}

class OMDBApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Environment environment = Environment(
        environmentName: "Dev",
        baseApiURL: "http://www.omdbapi.com/",
        devUserName: "devusername",
        devUserPassword: "devloginpassword",
        enableAutoLogin: true);
    environment.printEnvironment();
    return BlocProvider<AppBloc>(
        bloc: AppBloc(environment),
        child: MaterialApp(
          title: 'OMDB Demo App',
          theme: ThemeData(
            primarySwatch: Colors.blueGrey,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          home: Navigation(),
        ));
  }
}
