// Due to the size I will not write a class for this api response at the moment
// this could be needed at a later date.

///{
//    "Title":"Pokémon",
//    "Year":"1997–",
//    "Rated":"TV-Y",
//    "Released":"08 Sep 1998",
//    "Runtime":"24 min",
//    "Genre":"Animation, Action, Adventure, Comedy, Family, Fantasy",
//    "Director":"N/A",
//    "Writer":"Junichi Masuda, Ken Sugimori, Satoshi Tajiri",
//    "Actors":"Ikue Ôtani, Rica Matsumoto, Rodger Parsons, Kayzie Rogers",
//    "Plot":"Ash Ketchum, his yellow pet Pikachu, and his human friends explore a world of powerful creatures.",
//    "Language":"Japanese",
//    "Country":"Japan",
//    "Awards":"1 nomination.",
//    "Poster":"https://m.media-amazon.com/images/M/MV5BNjU1YjM2YzAtZWE2Ny00ZWNiLWFkZWItMDJhMzJiNDQwMmI4XkEyXkFqcGdeQXVyNTU1MjgyMjk@._V1_SX300.jpg",
//    "Ratings":[
//       {
//          "Source":"Internet Movie Database",
//          "Value":"7.5/10"
//       }
//    ],
//    "Metascore":"N/A",
//    "imdbRating":"7.5",
//    "imdbVotes":"37,812",
//    "imdbID":"tt0168366",
//    "Type":"series",
//    "totalSeasons":"23",
//    "Response":"True"
// }

// with variation in the following for movies.
//  "Metascore":"53",
//  "imdbRating":"6.6",
//  "imdbVotes":"138,507",
//  "imdbID":"tt5884052",
//  "Type":"movie",
//  "DVD":"23 Jul 2019",
//  "BoxOffice":"$144,105,346",
//  "Production":"Toho Company Ltd., Legendary Pictures",
//  "Website":"N/A",
//  "Response":"True"

class ObjectDetails {
  final String title;
  final String year;
  final String description;
  final String imdbId;

  ObjectDetails({this.title, this.year, this.description, this.imdbId});

  factory ObjectDetails.fromJson(Map<String, dynamic> json) {
    return ObjectDetails(
        title: json['Title'] as String,
        year: json['Year'] as String,
        description: json['Plot'] as String,
        imdbId: json['imdbID'] as String);
  }
}
