/// A data class used to hold returned json objects from OMDB
/// example
///   {
///         "Title":"Pokémon Detective Pikachu",
///         "Year":"2019",
///         "imdbID":"tt5884052",
///         "Type":"movie",
///         "Poster":"https://m.media-amazon.com/images/M/MV5BNDU4Mzc3NzE5NV5BMl5BanBnXkFtZTgwMzE1NzI1NzM@._V1_SX300.jpg"
///    },

class OMDBObject {
  final String title;
  final String year;
  final String imdbID;
  final String type;
  final String posterUrl;

  OMDBObject({
    this.title,
    this.year,
    this.imdbID,
    this.type,
    this.posterUrl,
  });

  factory OMDBObject.fromJson(Map<String, dynamic> json) {
    return OMDBObject(
      title: json['Title'] as String,
      year: json['Year'] as String,
      imdbID: json["imdbID"] as String,
      type: json['Type'] as String,
      posterUrl: json['Poster'] as String,
    );
  }
}
