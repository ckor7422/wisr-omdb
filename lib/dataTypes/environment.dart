/// The environment object is used to hold settings that affect the
/// functionality of the app.
/// An instance should be instantiated when the app is launched and passed
/// to the appBloc in order to be accessible everywhere in the app.
/// Settings in the environment such as baseApiURL should be set based on the
/// branch for example the username and password of a dev account can be set to
/// prefill in dev while the master branch that will deployed should not
/// contain these.
class Environment {
  final String environmentName;
  final String baseApiURL;
  final String debugFocus;
  final String devUserName;
  final String devUserPassword;
  final bool enableAutoLogin;

  Environment(
      {this.environmentName,
      this.baseApiURL,
      this.debugFocus,
      this.devUserName,
      this.devUserPassword,
      this.enableAutoLogin});

  void printEnvironment() {
    print("");
    print("----------Environment Settings----------");
    print("  baseApiURL: " + baseApiURL.toString());
    print("  debugFocus: " + debugFocus.toString());
    print("  environmentName: " + environmentName.toString());
    print("----------Environment Settings----------");
    print("");
  }

  factory Environment.fromJson(Map<String, dynamic> json) {
    return Environment(
      environmentName: json['enviroment_name'] as String,
      baseApiURL: json['base_api_url'] as String,
      debugFocus: json["debug_focus"] as String,
      devUserName: json['dev_user_name'] as String,
      devUserPassword: json['dev_user_password'] as String,
      enableAutoLogin: json['enable_drafts'] as bool,
    );
  }
}
