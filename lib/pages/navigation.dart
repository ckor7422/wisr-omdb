import 'package:flutter/material.dart';
import 'package:omdb/bloc/appBloc.dart';
import 'package:omdb/bloc/blocProvider.dart';
import 'package:omdb/bloc/navigationBloc.dart';
import 'package:omdb/pages/searchMovies.dart';

import 'searchSeries.dart';

/// The Navigation page is the first page that the application loads
/// It controls how main pages swap between each other (when not stacked)
class Navigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AppBloc appBloc = BlocProvider.of<AppBloc>(context);
    NavBloc navBloc = appBloc.nav;
    navBloc.setContext = context;
    return Scaffold(
        body: StreamBuilder(
            //The page the app first opens on is set here
            initialData: Pages.SearchMovies,
            stream: navBloc.pageStream,
            builder: (context, data) {
              switch (data.data) {
                // Pages returning null have not been implemented yet.
                case Pages.Onboarding:
                  return null;
                case Pages.Home:
                  return null;
                case Pages.Login:
                  return null;
                case Pages.SearchMovies:
                  return MovieSearchPage();
                case Pages.SearchSeries:
                  return SeriesSearchPage();
              }
              return null;
            }));
  }
}
