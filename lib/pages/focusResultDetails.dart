import 'package:flutter/material.dart';
import 'package:omdb/bloc/appBloc.dart';
import 'package:omdb/bloc/blocProvider.dart';
import 'package:omdb/bloc/favouriteBloc.dart';
import 'package:omdb/bloc/movieListModal.dart';
import 'package:omdb/bloc/searchBloc.dart';
import 'package:omdb/dataTypes/objectDetails.dart';

// This page shows the user the plot of the omdb object
// they have previously selected while also still having a usable favourites
// icon that updates the rest of the app to indicate any changes to it.
class FocusResultPage extends StatefulWidget {
  FocusResultPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _FocusResultPageState createState() => _FocusResultPageState();
}

class _FocusResultPageState extends State<FocusResultPage> {
  @override
  Widget build(BuildContext context) {
    AppBloc appBloc = BlocProvider.of<AppBloc>(context);
    SearchBloc searchBloc = appBloc.searchBloc;
    FavouriteBloc favouriteBloc = appBloc.favouriteBloc;

    MoviesListViewModel moviesListViewModel = appBloc.moviesListViewModel;

    ObjectDetails details = searchBloc.getSelectedResultDetails;

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                child: Column(
              children: [
                StreamBuilder(
                    // stream: appBloc.searchBloc.statusStream,
                    stream: moviesListViewModel.moviesUserFavouritesStream(),
                    initialData: false,
                    builder: (context, stream) {
                      return GestureDetector(
                        onTap: () {
                          if (favouriteBloc.isFavourited(details.imdbId)) {
                            favouriteBloc.removeFavourite(details.imdbId);
                            searchBloc.pushStream();
                          } else {
                            favouriteBloc.addFavourite(details.imdbId);
                            searchBloc.pushStream();
                          }
                        },
                        child: (favouriteBloc.isFavourited(details.imdbId))
                            ? Icon(Icons.favorite)
                            : Icon(Icons.favorite_outline),
                      );
                    }),
                Text(details.title),
                Text(details.year),
                Text(details.description),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
