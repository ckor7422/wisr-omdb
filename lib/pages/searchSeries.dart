import 'package:flutter/material.dart';
import 'package:omdb/bloc/appBloc.dart';
import 'package:omdb/bloc/blocProvider.dart';
import 'package:omdb/bloc/favouriteBloc.dart';
import 'package:omdb/bloc/movieListModal.dart';
import 'package:omdb/bloc/searchBloc.dart';
import 'package:omdb/common/sideBar.dart';
import 'package:omdb/pages/focusResultDetails.dart';

// This page lets the user search series by title and returns displays a
// list of results retuend to the user.
// The list has a favourite icon that can be pressed for each row that saves
// the object as a favourite
class SeriesSearchPage extends StatefulWidget {
  SeriesSearchPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SeriesSearchPageState createState() => _SeriesSearchPageState();
}

class _SeriesSearchPageState extends State<SeriesSearchPage> {
  @override
  Widget build(BuildContext context) {
    AppBloc appBloc = BlocProvider.of<AppBloc>(context);
    SearchBloc searchBloc = appBloc.searchBloc;
    FavouriteBloc favouriteBloc = appBloc.favouriteBloc;
    MoviesListViewModel moviesListViewModel = appBloc.moviesListViewModel;

    TextEditingController searchField = TextEditingController();

    return Scaffold(
      drawer: sideBar(context),
      appBar: AppBar(
        title: Text("Series"),
      ),
      body: Center(
        child: SingleChildScrollView(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.all(10),
                child: TextField(
                  controller: searchField,
                )),
            MaterialButton(
                color: Colors.blueGrey,
                child: Text("Search"),
                textColor: Colors.white,
                onPressed: () => {searchBloc.populateSearchResults(searchField.text, "series")}),
            Container(
                padding: EdgeInsets.only(left: 24, right: 24),
                child: StreamBuilder(
                    // stream: appBloc.searchBloc.statusStream,
                    stream: moviesListViewModel.moviesUserFavouritesStream(),
                    initialData: false,
                    builder: (context, stream) {
                      print(stream.connectionState);
                      return Container(
                        child: (searchBloc.getSearchResults != null)
                            ? ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: searchBloc.getSearchResults.length,
                                itemBuilder: (BuildContext context, int index) {
                                  FavouritedMovies currentResult = stream.data[index];

                                  return (currentResult.movie.title == "No Result")
                                      ? Container(child: Text("No Result"))
                                      : Container(
                                          height: 40,
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              GestureDetector(
                                                onTap: () async {
                                                  searchBloc.setSelectedResult = currentResult.movie;
                                                  bool success = await searchBloc.getSpecificObjectDetails();
                                                  if (success) {
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) => FocusResultPage(
                                                                title: currentResult.movie.title,
                                                              )),
                                                    );
                                                  }
                                                },
                                                child: Text(currentResult.movie.title),
                                              ),
                                              GestureDetector(
                                                onTap: () {
                                                  if (favouriteBloc.isFavourited(currentResult.movie.imdbID)) {
                                                    favouriteBloc.removeFavourite(currentResult.movie.imdbID);
                                                  } else {
                                                    favouriteBloc.addFavourite(currentResult.movie.imdbID);
                                                  }
                                                },
                                                child: (favouriteBloc.isFavourited(currentResult.movie.imdbID))
                                                    ? Icon(Icons.favorite)
                                                    : Icon(Icons.favorite_outline),
                                              )
                                            ],
                                          ));
                                })
                            : Container(),
                      );
                    }))
          ],
        )),
      ),
    );
  }
}
